Feature: onliner catalog feature

  Scenario: checking products in the catalog for a specific filter for a specific category
    Given Chrome browser blank page
    When I open "https://onliner.by" on blank page
    Then site is opened

    When I click "Каталог" on Main page
    Then Catalog page is opened

    When I click "Электроника" and "Телевидение и видео" and "Телевизоры" on Catalog page
    Then Search page is opened

    When I check checkbox near "samsung" on Vendor section and enter "1000" in Under field in Minimum price in store offers section And select "40" and "50" from dropdowns in Diagonal section And check checkbox near "1920x1080" (Full HD) on Resolution section
    Then all search results that are displayed on Search page meet all of the search criteria

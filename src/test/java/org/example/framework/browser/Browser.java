package org.example.framework.browser;


import com.google.common.base.Strings;
import org.example.framework.utils.LocaleManager;
import org.example.framework.utils.Logger;
import org.example.framework.utils.PropertiesManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.naming.NamingException;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.remote.BrowserType.CHROME;


public final class Browser {

    public enum BrowsersType {
        CHROME("chrome"),
        FIREFOX("firefox");
        private final String browser;

        BrowsersType(String browser) {
            this.browser = browser;
        }

        public String getString() {
            return browser;
        }
    }

    private static final long IMPLICITLY_WAIT = 10;
    private static final String DEFAULT_CONDITION_TIMEOUT = "defaultConditionTimeout";
    private static final String DEFAULT_PAGE_LOAD_TIMEOUT = "defaultPageLoadTimeout";

    static final String MAIN_PROPERTIES_FILE = "main.properties";
    private final BrowsersType DEFAULT_BROWSER_TYPE = BrowsersType.valueOf(CHROME.toUpperCase());
    private static final String BROWSER_TYPE = "browser";

    private static Browser instance;
    private RemoteWebDriver driver;
    public BrowsersType currentBrowser;
    public static org.example.framework.utils.PropertiesManager PropertiesManager;

    private String timeoutForCondition;
    private String timeoutForPageLoad;

    private Browser() {
        initMainSettings();
        try {
            driver = BrowsersFactory.setUpBrowser(currentBrowser.toString());
            driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        } catch (NamingException e) {
            Logger.getInstance().error(e.getMessage());
            e.printStackTrace();
        }
        Logger.getInstance().info(String.format(LocaleManager.getInstance().getProperty("loc.browser.ready"), currentBrowser.toString()));
    }

    public static synchronized Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public boolean isBrowserExists() {
        return instance != null;
    }

    public Integer getTimeoutForCondition() {
        return Integer.valueOf(timeoutForCondition);
    }

    public Integer getTimeoutForPageLoad() {

        return Integer.valueOf(timeoutForPageLoad);
    }

    public void navigateToURL(String url) {
        driver.navigate().to(url);
    }

    public RemoteWebDriver getDriver() {
        return driver;
    }

    public void exit() {
        try {
            driver.quit();
            Logger.getInstance().info(LocaleManager.getInstance().getProperty("loc.browser.quit"));
        } catch (Exception e) {
            Logger.getInstance().error(e.getMessage());
            e.printStackTrace();
        } finally {
            instance = null;
        }
    }



    private void initMainSettings() {
        PropertiesManager = new PropertiesManager(MAIN_PROPERTIES_FILE);
        timeoutForCondition = PropertiesManager.getProperty(DEFAULT_CONDITION_TIMEOUT);
        timeoutForPageLoad = PropertiesManager.getProperty(DEFAULT_PAGE_LOAD_TIMEOUT);

        if (Strings.isNullOrEmpty(PropertiesManager.getProperty(BROWSER_TYPE))) {
            String browser = System.getProperty(BROWSER_TYPE).toUpperCase();
            if (!Strings.isNullOrEmpty(browser)) {
                currentBrowser = BrowsersType.valueOf(browser);
            } else {
                currentBrowser = DEFAULT_BROWSER_TYPE;
            }
        } else {
            currentBrowser = BrowsersType.valueOf(PropertiesManager.getProperty(BROWSER_TYPE).toUpperCase());
        }
    }
}

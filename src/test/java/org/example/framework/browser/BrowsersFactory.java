package org.example.framework.browser;

import org.example.framework.browser.Browser.BrowsersType;
import org.example.framework.utils.LocaleManager;
import org.example.framework.utils.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;


public abstract class BrowsersFactory {

    public static RemoteWebDriver setUpBrowser(final BrowsersType type) {
        DesiredCapabilities capabilitiesProxy;
        RemoteWebDriver driver = null;
        File driverFile;

        switch (type) {
            case CHROME:
                capabilitiesProxy = DesiredCapabilities.chrome();
                driverFile = getDriverFile("chromedriver");
                System.setProperty("webdriver.chrome.driver", driverFile.getAbsolutePath());
                driver = new ChromeDriver(capabilitiesProxy);
                driver.manage().window().maximize();
                break;
            case FIREFOX:
                capabilitiesProxy = DesiredCapabilities.firefox();
                driverFile = getDriverFile("geckodriver");
                System.setProperty("webdriver.gecko.driver", driverFile.getAbsolutePath());
                driver = new FirefoxDriver(capabilitiesProxy);
                driver.manage().window().maximize();
                break;
        }
        return driver;
    }

    private static File getDriverFile(String driverName) {
        URL driverURL;
        File driverFile = null;
        driverURL = ClassLoader.getSystemResource(driverName);
        try {
            driverFile = new File(driverURL.toURI());
            Path path = Paths.get(driverFile.getAbsolutePath());
            Set<PosixFilePermission> perms = Files.readAttributes(path, PosixFileAttributes.class).permissions();

            System.out.format("Permissions before: %s%n",  PosixFilePermissions.toString(perms));

            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            perms.add(PosixFilePermission.GROUP_WRITE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.GROUP_EXECUTE);
            perms.add(PosixFilePermission.OTHERS_WRITE);
            perms.add(PosixFilePermission.OTHERS_READ);
            perms.add(PosixFilePermission.OTHERS_EXECUTE);
            Files.setPosixFilePermissions(path, perms);

            System.out.format("Permissions after:  %s%n",  PosixFilePermissions.toString(perms));
        } catch (URISyntaxException | IOException e) {
            Logger.getInstance().error(e.getMessage());
        }
        return driverFile;
    }

    public static RemoteWebDriver setUpBrowser(final String type) throws NamingException {
        for (BrowsersType browserType : BrowsersType.values()) {
            if (browserType.toString().equalsIgnoreCase(type)) {
                return setUpBrowser(browserType);
            }
        }
        throw new NamingException(LocaleManager.getInstance().getProperty("loc.browser.name.wrong"));
    }
}

package org.example.framework.core;

import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

public abstract class BaseTest extends BaseEntity {

	public abstract void runTest() throws IOException;

	@BeforeClass
	public void before(ITestContext context) {
		this.context = context;
	}

	@AfterClass
	public void after() {
		if (browser.isBrowserExists()) {
			browser.exit();
		}
	}

}

package org.example.framework.core;

import io.cucumber.testng.FeatureWrapper;
import io.cucumber.testng.PickleWrapper;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

public abstract class BaseTestCucumber extends BaseEntity {

	public abstract void  scenario(PickleWrapper pickle, FeatureWrapper cucumberFeature) ;

	@BeforeClass
	public void before(ITestContext context) {
		this.context = context;
	}

	@AfterClass
	public void after() {
		if (browser.isBrowserExists()) {
			browser.exit();
		}
	}

}

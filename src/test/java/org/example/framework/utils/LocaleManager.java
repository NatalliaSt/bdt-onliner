package org.example.framework.utils;

import com.google.common.base.Strings;

public class LocaleManager {

    public enum Locale {
        EN("en"),
        RU("ru");
        private String locale;

        Locale(String locale) {
            this.locale = locale;
        }

        public String toString() {
            return locale;
        }
    }

    private static final Locale DEFAULT_LOCALE = Locale.EN;
    private static final String SYSTEM_LOCALE = "locale";
    private static LocaleManager instance = null;
    private PropertiesManager localePropertiesManager;

    private LocaleManager() {
        String locale = System.getProperty(SYSTEM_LOCALE);
        if (Strings.isNullOrEmpty(locale)) {
            locale = DEFAULT_LOCALE.toString();
        }
        localePropertiesManager = new PropertiesManager(String.format("localization/loc_%1$s.properties", DEFAULT_LOCALE.toString().toLowerCase()), String.format("localization/loc_%1$s.properties", locale.toLowerCase()));
        Logger.getInstance().info(String.format(getProperty("loc.locale"), locale));
    }

    public static synchronized LocaleManager getInstance() {
        if (instance == null) {
            instance = new LocaleManager();
        }
        return instance;
    }

    public String getProperty(final String key) {
        return localePropertiesManager.getProperty(key);
    }
}

package org.example.framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;

public class CheckBox extends BaseUIElement {

	public CheckBox(final By locator, final String name) {
		super(locator, name);
	}
	public CheckBox(final RemoteWebElement element, final String name ) {
		super(element, name);
	}


}

package org.example.framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;

public class Label extends BaseUIElement {

	public Label(final By locator, final String name) {
		super(locator, name);
	}

	public Label(final RemoteWebElement element, final String name ) {
		super(element, name);
	}
}

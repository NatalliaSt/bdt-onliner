package org.example.framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;

public class Select extends BaseUIElement {
    /**
     *
     * @param locator
     * @param name
     */
    public Select(final By locator, final String name) {
        super(locator, name);
    }
    public Select(final RemoteWebElement element, final String name ) {
        super(element, name);
    }


    /**
     * Select item.
     */
    public void selectByVisibleText(String selectItem) {
        info(getLocale("loc.action.select"));
        waitElementDisplayed();
        new org.openqa.selenium.support.ui.Select(this.element).selectByVisibleText(selectItem);
    };

}

package org.example.framework.elements;

import org.openqa.selenium.By;

public class Input extends BaseUIElement {

	public Input(final By locator, final String name) {
		super(locator, name);
	}

	public void fillFieldValue(String value) {
		info(getLocale("loc.action.fillFieldValue"));
		waitElementDisplayed();
		this.element.sendKeys(value);
	}


}

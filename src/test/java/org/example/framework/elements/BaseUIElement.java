package org.example.framework.elements;

import org.example.framework.core.BaseEntity;
import org.example.framework.utils.INameElementForLogger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public abstract class BaseUIElement extends BaseEntity implements INameElementForLogger {

    protected String name;
    protected By locator;
    protected RemoteWebElement element = null;

    protected BaseUIElement(final By locator, final String name) {
        this.locator = locator;
        this.name = name;
    }

    protected BaseUIElement(final RemoteWebElement element, final String name) {
        this.element = element;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public RemoteWebElement getElement() {
        waitElementDisplayed();
        return element;
    }

    public void waitElementDisplayed() {
        isDisplayed();
        Assert.assertTrue(element.isDisplayed());
    }

    public void moveToElement() {
        RemoteWebElement el = getElement();
        if (browser.getDriver() instanceof JavascriptExecutor) {
            Coordinates coordinates = el.getCoordinates();
            ((JavascriptExecutor) browser.getDriver()).executeScript(String.format("scroll(0, %s)", coordinates.onPage().getY()), el);
        }
    }

    public void click() {
        info(getLocale("loc.action.click"));
        getElement().click();
    }

    public void waitStalenessOf() {
        try {
            browser.getDriver().manage().timeouts().implicitlyWait(browser.getTimeoutForCondition(), TimeUnit.SECONDS);
            element = (RemoteWebElement) browser.getDriver().findElement(locator);
            if (element.isDisplayed()) {
                WebDriverWait wait = new WebDriverWait(browser.getDriver(), browser.getTimeoutForPageLoad());
                wait.until(ExpectedConditions.stalenessOf(this.element));
            }
        } catch (Exception e) {
            warn(e.getMessage());
        }
    }

    public void clickAndWait() {
        click();
        waitForPageToLoad();
    }

    public void waitForPageToLoad() {
        WebDriverWait wait = new WebDriverWait(browser.getDriver(),
                browser.getTimeoutForPageLoad());
        try {
            wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
                public Boolean apply(final WebDriver d) {
                    if (!(d instanceof JavascriptExecutor)) {
                        return true;
                    }
                    Object result = ((JavascriptExecutor) d)
                            .executeScript("return document['readyState'] ? 'complete' == document.readyState : true");
                    if (result != null && result instanceof Boolean
                            && (Boolean) result) {
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            warn(e.getMessage());
        }
    }


    public boolean isDisplayed() {
        try {
            if (element == null) {
                browser.getDriver().manage().timeouts().implicitlyWait(browser.getTimeoutForCondition(), TimeUnit.SECONDS);
                element = (RemoteWebElement) browser.getDriver().findElement(locator);
            }
            return element.isDisplayed();
        } catch (Exception e) {
            warn(e.getMessage());
        }
        return false;
    }

    public String getText() {
        return getElement().getText();
    }


}


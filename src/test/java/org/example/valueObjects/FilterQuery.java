package org.example.valueObjects;

public class FilterQuery {
    public String priceTo;
    public String vendor;
    public String resolution;
    public String diagonalMin;
    public String diagonalMax;


    public FilterQuery(String priceTo, String vendor, String resolution, String diagonalMin, String diagonalMax) {
        this.priceTo = priceTo;
        this.vendor = vendor;
        this.resolution = resolution;
        this.diagonalMin = diagonalMin;
        this.diagonalMax = diagonalMax;
    }
}

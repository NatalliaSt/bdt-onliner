package org.example.pages;

import org.example.framework.core.BasePage;
import org.example.framework.elements.Button;
import org.openqa.selenium.By;

public class MainPage extends BasePage {
    private static String pageLocator ="//body/div[@class='layout-container']";
    private String xpathMenuItem = "//span[text()='%s' and @class='b-main-navigation__text']/..";

    public MainPage() {
        super(By.xpath(pageLocator), "mane page");
    }

    public void openMenuItem(String menuItemName) {
        Button btnMenuItem = new Button(By.xpath(String.format(xpathMenuItem,menuItemName)),"Menu Item select");
        btnMenuItem.clickAndWait();
    }
}

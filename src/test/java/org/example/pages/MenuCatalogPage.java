package org.example.pages;

import org.example.framework.core.BasePage;
import org.example.framework.elements.Button;
import org.openqa.selenium.By;

public class MenuCatalogPage extends BasePage {
    private static String pageLocator = "//h1[@class='catalog-navigation__title']";
    private String xpathMenuItemLocator = "//span[text()='%s']";
    private String xpathSubMenuItemLocator = "//div[contains(text(),'%s')]";
    private String xpathSubSubMenuItemLocator = "//span[contains(text(),'%s')]";

    public MenuCatalogPage() {
        super(By.xpath(pageLocator), "menu catalog page");
    }

    public void chooseMenu(String menuItem, String subMenuItem, String subSubMenuItem) {
        chooseMenuItem(menuItem);
        chooseSubMenuItem(subMenuItem);
        chooseSubSubMenuItem(subSubMenuItem);
    }

    private void chooseMenuItem(String item) {
        Button btnItem = new Button(By.xpath(String.format(xpathMenuItemLocator,item)), "menu item");
        btnItem.click();
    }

    private void chooseSubMenuItem(String item) {
        Button btnSubItem = new Button(By.xpath(String.format(xpathSubMenuItemLocator,item)), "subMenu item");
        btnSubItem.click();
    }

    private void chooseSubSubMenuItem(String item) {
        Button btnSubSubItem = new Button(By.xpath(String.format(xpathSubSubMenuItemLocator,item)), "subSubMenu item");
        btnSubSubItem.clickAndWait();
    }
}

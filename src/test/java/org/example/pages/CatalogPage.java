package org.example.pages;

import org.example.framework.core.BasePage;
import org.example.framework.elements.*;
import org.example.valueObjects.FilterQuery;

import org.openqa.selenium.By;


import java.util.List;
import java.util.Locale;

import static org.testng.Assert.assertTrue;

public class CatalogPage extends BasePage {
    private static String pageLocator = "//h1[@class='schema-header__title']";
    private Input inputPriceTo = new Input(By.xpath("//span[contains(text(),'Минимальная цена в предложениях магазинов')]/../following-sibling::div/descendant::input[@placeholder='до']"), "input price to");
    private String xpathCheckBoxVendor = "//div[@class='schema-filter__facet']/descendant::input[@value='%s'][1]/ancestor::label";
    private String xpathCheckBoxResolution = "//div[@class='schema-filter__facet']//input[@value='%s'][1]/ancestor::label";
    private Select selectSizeMin = new Select(By.xpath("//select[contains(@data-bind,'facet.value.from')]"), "select size min");
    private Select selectSizeMax = new Select(By.xpath("//select[contains(@data-bind,'facet.value.to')]"), "select size max");
    private Label labelStalenessElement = new Label(By.xpath("//div[@class='schema-product__group'][1]"), "Staleness Element");
    private String xpathProductDescription = "//span[contains(@data-bind,'product.description')]";
    private String xpathDataPrices = "//span[contains(@data-bind,'data.prices')]";
    private String xpathProductFullName = "//span[@data-bind='html: product.extended_name || product.full_name']";

    public CatalogPage() {
        super(By.xpath(pageLocator), "catalog page");
    }

    public void setFilter(FilterQuery filterQuery) {
        CheckBox checkBoxVendor = new CheckBox(By.xpath(String.format(xpathCheckBoxVendor, filterQuery.vendor)), "checkBox Vendor");
        CheckBox checkBoxResolution = new CheckBox(By.xpath(String.format(xpathCheckBoxResolution, filterQuery.resolution)), "checkBox Resolution");

        checkBoxVendor.moveToElement();
        checkBoxVendor.click();
        labelStalenessElement.waitStalenessOf();

        inputPriceTo.fillFieldValue(filterQuery.priceTo);
        labelStalenessElement.waitStalenessOf();

        checkBoxResolution.moveToElement();
        checkBoxResolution.click();
        labelStalenessElement.waitStalenessOf();

        selectSizeMin.moveToElement();
        selectSizeMin.selectByVisibleText(String.format("%s\"", filterQuery.diagonalMin));
        labelStalenessElement.waitStalenessOf();
        selectSizeMax.selectByVisibleText(String.format("%s\"", filterQuery.diagonalMax));

        labelStalenessElement.waitStalenessOf();
    }

    public void assertSearchResult(FilterQuery filterQuery) {
        List<Label> listOfElementsVendor = CollectionUIElements.getCollectionUIElements(By.xpath(xpathProductFullName), "Vendor Label", Label.class);
        List<Label> listOfElementsPrices = CollectionUIElements.getCollectionUIElements(By.xpath(xpathDataPrices), "Prices Label", Label.class);
        List<Label> listOfElementsDescription = CollectionUIElements.getCollectionUIElements(By.xpath(xpathProductDescription), "Description Label", Label.class);
        listOfElementsVendor.forEach(item -> {
            String str = item.getText().toLowerCase(Locale.ROOT);
            assertTrue(str.contains(filterQuery.vendor));
            info(item.getText());
        });
        listOfElementsPrices.forEach(item -> {
            String str = item.getText();
            str = str.replace(" р.", "");
            str = str.replace(",", ".");

            float price = Float.parseFloat(str);

            Integer priceCoin = Math.round(price * 100);
            Integer toPriceCoin = Math.round(Float.parseFloat(filterQuery.priceTo) * 100);
            info(priceCoin.toString());
            info(toPriceCoin.toString());
            assertTrue((priceCoin < toPriceCoin));
        });
        listOfElementsDescription.forEach(item -> {
            info(item.getText());
            String str = item.getText().toLowerCase(Locale.ROOT);
            assertTrue(str.contains(filterQuery.resolution));

            int indexInch = str.indexOf('"');
            if (indexInch > 0) {
                str = str.substring(0, indexInch);
                float inch = Float.parseFloat(str);
                assertTrue(inch < Float.parseFloat(filterQuery.diagonalMax) || inch > Float.parseFloat(filterQuery.diagonalMin));
            } else {
                assertTrue(false);
            }
            info(str);

        });
    }
}

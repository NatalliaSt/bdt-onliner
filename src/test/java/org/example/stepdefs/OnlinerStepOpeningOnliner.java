package org.example.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.framework.core.BaseEntity;
import org.example.pages.MainPage;
import org.example.pages.MenuCatalogPage;

public class OnlinerStepOpeningOnliner extends BaseEntity {
    private MainPage mainPage;
    private MenuCatalogPage menuCatalogPage;

    @Given("Chrome browser blank page")
    public void preOpeningOnliner() {
        logStep(1);
    }

    @When("I open {string} on blank page")
    public void doOpeningOnliner(String url) {
        browser.navigateToURL(url);
    }

    @Then("site is opened")
    public void postOpeningOnliner() {

        mainPage = new MainPage();
    }
}

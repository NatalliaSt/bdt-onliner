package org.example.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.framework.core.BaseEntity;
import org.example.pages.MainPage;
import org.example.pages.MenuCatalogPage;

public class OnlinerStepChooseMenuItem extends BaseEntity {
    private MainPage mainPage;
    private MenuCatalogPage menuCatalogPage;

    @When("I click {string} on Main page")
    public void doChooseMenuItem(String name) {
        logStep(2);
        mainPage = new MainPage();
        mainPage.openMenuItem(name);
    }

    @Then("Catalog page is opened")
    public void postChooseMenuItem() {
        menuCatalogPage = new MenuCatalogPage();
    }
}

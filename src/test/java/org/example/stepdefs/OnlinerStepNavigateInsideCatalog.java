package org.example.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.framework.core.BaseEntity;
import org.example.pages.CatalogPage;
import org.example.pages.MenuCatalogPage;

public class OnlinerStepNavigateInsideCatalog extends BaseEntity {
    private MenuCatalogPage menuCatalogPage;
    private CatalogPage catalogPage;

    @When("I click {string} and {string} and {string} on Catalog page")
    public void doNavigateInsideCatalog(String menuItem, String subMenuItem, String subSubMenuItem) {
        logStep(3);
        menuCatalogPage = new MenuCatalogPage();
        menuCatalogPage.chooseMenu(menuItem, subMenuItem, subSubMenuItem);
    }
    @Then("Search page is opened")
    public void postNavigateInsideCatalog() {
        catalogPage = new CatalogPage();
    }
}

package org.example.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.framework.core.BaseEntity;
import org.example.pages.CatalogPage;
import org.example.valueObjects.FilterQuery;

public class OnlinerStepSelectByParameters extends BaseEntity {
    private CatalogPage catalogPage;
    private FilterQuery filterQuery;

    @When("I check checkbox near {string} on Vendor section and enter {string} in Under field in Minimum price in store offers section And select {string} and {string} from dropdowns in Diagonal section And check checkbox near {string} \\(Full HD) on Resolution section")
    public void doSelectByParameters(String vendor, String priceTo, String diagonalMin, String diagonalMax, String resolution) {
        logStep(4);
        catalogPage = new CatalogPage();
        filterQuery = new FilterQuery(priceTo,
                vendor,
                resolution,
                diagonalMin,
                diagonalMax
        );
        catalogPage.setFilter(filterQuery);
    }

    @Then("all search results that are displayed on Search page meet all of the search criteria")
    public void postSelectByParameters() {
        logStep(5);
        catalogPage.assertSearchResult(filterQuery);
    }
}

